# K3S (Lightweight Kubernetes)
The certified Kubernetes distribution built for IoT & Edge computing
More can be found here: https://k3s.io/

K3s related stuff can be found here.

## Install K3S
```
curl -sfL https://get.k3s.io | sh - 
# Check for Ready node, takes ~30 seconds 
k3s kubectl get node 
```
